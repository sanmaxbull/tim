package com.tims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.tims.*")
@EnableAutoConfiguration
public class TrainingInstituteWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingInstituteWsApplication.class, args);
	}

}
