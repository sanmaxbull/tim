package com.tims.constants;

public interface CommonConstants {

	public static final String success = "Success";
	
	public static final String fail = "Fail";

	public static final String savedSuccessfully = "Saved Successfully";
	
	public static final String notSavedSuccessfully = "Not Saved Successfully";
	
	public static final String notFound = "Not Found";
	
	public static final String found = "Found";
	
	public static final String deleted = "Deleted";
	
	public static final String deletedSuccesfully = "Deleted Successfully";



	
	

}
