package com.tims.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "registration_candidate_details")
public class RegistrationCandidateDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer registrationCandidateId;
	public String email;
	public String name;
	public String primaryPhone;
	public String alternatePhone;
	public Integer courseOptedFor;
	public String qualification;
	public Integer firstTimeToAttend;
	public Integer organisationId;
	public Integer userId;
	public Boolean isActive;
	public String createdBy;
	@Column(name = "CREATED_ON")
	@Temporal(TemporalType.TIMESTAMP)
	public Date createdOn;
	@Column(name = "UPDATED_BY")
	public String updatedBy;
	@Column(name = "UPDATED_ON")
	@Temporal(TemporalType.TIMESTAMP)
	public Date updatedOn;

	public Integer getRegistrationCandidateId() {
		return registrationCandidateId;
	}

	public void setRegistrationCandidateId(Integer registrationCandidateId) {
		this.registrationCandidateId = registrationCandidateId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getAlternatePhone() {
		return alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public Integer getCourseOptedFor() {
		return courseOptedFor;
	}

	public void setCourseOptedFor(Integer courseOptedFor) {
		this.courseOptedFor = courseOptedFor;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Integer getFirstTimeToAttend() {
		return firstTimeToAttend;
	}

	public void setFirstTimeToAttend(Integer firstTimeToAttend) {
		this.firstTimeToAttend = firstTimeToAttend;
	}
	
	public Integer getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Integer organisationId) {
		this.organisationId = organisationId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
}
