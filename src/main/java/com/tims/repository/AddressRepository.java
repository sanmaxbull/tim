package com.tims.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tims.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>{

}
