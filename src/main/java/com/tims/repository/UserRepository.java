package com.tims.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tims.model.TimsUser;
@Repository
public interface UserRepository extends JpaRepository<TimsUser, Integer> {

	@SuppressWarnings("unchecked")
	TimsUser save(TimsUser user);
	
	


}
