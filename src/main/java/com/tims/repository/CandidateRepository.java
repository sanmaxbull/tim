package com.tims.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.tims.model.RegistrationCandidateDetails;

public interface CandidateRepository extends PagingAndSortingRepository<RegistrationCandidateDetails, Integer> {

	Page<?> findAllAndByOrganisationIdAndIsActiveOrderByUpdatedOnDesc(Integer organisationId,
			Boolean isActive, Pageable page);

	RegistrationCandidateDetails findByRegistrationCandidateId(Integer registrationCandidateId);

	Page<?> findAllAndByOrganisationIdAndIsActiveAndEmailOrNameContaining(
			Integer organisationId, Boolean isActive, String search, String search1, Pageable page);

}
