package com.tims.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    
    public static Date getTodayDateTime() throws ParseException {
        Calendar currentDate = Calendar.getInstance();

        SimpleDateFormat formatter = new SimpleDateFormat(
                "MM/dd/yyyy HH:mm:ss a");

        Date todayDateTime = (Date) formatter.parse(formatter.format(currentDate
                .getTime()));
        return todayDateTime;
    }

}
