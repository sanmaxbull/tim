package com.tims.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tims.constants.CommonConstants;
import com.tims.constants.ListResponse;
import com.tims.constants.Response;
import com.tims.model.Address;
import com.tims.repository.AddressRepository;
import com.tims.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	AddressRepository addressRepository;

	@Override
	public Response saveAddress(Address address) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		try {

			Address addressDb = addressRepository.save(address);
			if (addressDb != null) {
				response.setData(addressDb);
				status.setMessage(CommonConstants.savedSuccessfully);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(addressDb);
				status.setMessage(CommonConstants.notSavedSuccessfully);
				status.setSuccess(CommonConstants.fail);
			}
			response.setStatus(status);

		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		return response;
	}

	@Override
	public Response getAddress(Integer addressId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		Optional<Address> addressDb = addressRepository.findById(addressId);
		if (addressDb != null) {
			response.setData(addressDb);
			status.setMessage(CommonConstants.found);
			status.setSuccess(CommonConstants.success);
		}
		response.setStatus(status);
		return response;
	}

	@Override
	public Response deleteAddressIdPermanent(Integer addressId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		addressRepository.deleteById(addressId);
		status.setMessage(CommonConstants.deletedSuccesfully);
		status.setSuccess(CommonConstants.success);
		response.setStatus(status);
		return response;
	}

	@Override
	public Response deleteAddressList(List<Integer> addressIdList) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		addressIdList.forEach(id -> addressRepository.deleteById(id));
		status.setMessage(CommonConstants.deletedSuccesfully);
		status.setSuccess(CommonConstants.success);
		response.setStatus(status);
		return response;
	}

	@Override
	public ListResponse getCandidateAddressList() {
		ListResponse response = new ListResponse();
		ListResponse.Status status = new ListResponse.Status();
		try {
			List<Address> addressListDb = addressRepository.findAll();
			if (!addressListDb.isEmpty() && addressListDb.size() > 0 && addressListDb != null) {
				response.setData(addressListDb);
				status.setMessage(CommonConstants.found);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(addressListDb);
				status.setMessage(CommonConstants.notFound);
				status.setSuccess(CommonConstants.fail);
			}
			response.setTotalResult(addressListDb.size());

		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		response.setStatus(status);
		return (response);
	}

}
