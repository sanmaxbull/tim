package com.tims.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tims.constants.CommonConstants;
import com.tims.constants.Response;
import com.tims.model.RegistrationCandidateDetails;
import com.tims.repository.CandidateRepository;
import com.tims.service.CandidateService;
import com.tims.utils.DateUtil;

@Service
public class CandidateServiceImpl implements CandidateService {

	@Autowired
	CandidateRepository candidateRepository;

	@Override
	public Response registerCandidate(RegistrationCandidateDetails registrationCandidateDetails) {

		Response response = new Response();
		Response.Status status = new Response.Status();
		try {
			if (registrationCandidateDetails.getRegistrationCandidateId() == null) {
				registrationCandidateDetails.setCreatedOn(DateUtil.getTodayDateTime());
			}
			registrationCandidateDetails.setUpdatedOn(DateUtil.getTodayDateTime());

			RegistrationCandidateDetails registrationCandidateDetailsDbObject = candidateRepository
					.save(registrationCandidateDetails);
			if (registrationCandidateDetailsDbObject != null) {
				response.setData(registrationCandidateDetailsDbObject);
				status.setMessage(CommonConstants.savedSuccessfully);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(registrationCandidateDetailsDbObject);
				status.setMessage(CommonConstants.notSavedSuccessfully);
				status.setSuccess(CommonConstants.fail);
			}
			response.setStatus(status);

		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		return response;
	}

	@Override
	public Response getAllRegisterCandidate(Boolean isActive, Integer organisationId, String search, Integer start,
			Integer limit) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		Page<?> registrationCandidateDetailsList;
		try {
			Pageable pageable = PageRequest.of(start, limit);
			if ((search != null) && (!search.isEmpty())) {
				registrationCandidateDetailsList = candidateRepository
						.findAllAndByOrganisationIdAndIsActiveAndEmailOrNameContaining(organisationId, isActive, search,
								search, pageable);
			} else {
				registrationCandidateDetailsList = candidateRepository
						.findAllAndByOrganisationIdAndIsActiveOrderByUpdatedOnDesc(organisationId, isActive, pageable);
			}
			if (!registrationCandidateDetailsList.isEmpty() && registrationCandidateDetailsList.getSize() > 0
					&& registrationCandidateDetailsList != null) {
				response.setData(registrationCandidateDetailsList);
				status.setMessage(CommonConstants.found);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(registrationCandidateDetailsList);
				status.setMessage(CommonConstants.notFound);
				status.setSuccess(CommonConstants.fail);
			}
		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		response.setStatus(status);
		return (response);
	}

	@Override
	public Response getRegistrationCandidate(Integer registrationCandidateId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		RegistrationCandidateDetails registrationCandidatObj = candidateRepository
				.findByRegistrationCandidateId(registrationCandidateId);
		if (registrationCandidatObj != null) {
			response.setData(registrationCandidatObj);
			status.setMessage(CommonConstants.found);
			status.setSuccess(CommonConstants.success);
		}
		response.setStatus(status);
		return response;
	}

	@Override
	public Response deleteRegisterCandidateList(List<RegistrationCandidateDetails> registrationCandidateDetailsList) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		candidateRepository.deleteAll(registrationCandidateDetailsList);
		status.setMessage(CommonConstants.deletedSuccesfully);
		status.setSuccess(CommonConstants.success);
		response.setStatus(status);
		return response;
	}

	@Override
	public Response deleteRegisterCandidateId(Integer registrationCandidateId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		candidateRepository.deleteById(registrationCandidateId);
		status.setMessage(CommonConstants.deletedSuccesfully);
		status.setSuccess(CommonConstants.success);
		response.setStatus(status);
		return response;
	}

}
