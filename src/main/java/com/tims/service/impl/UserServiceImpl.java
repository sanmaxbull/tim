package com.tims.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tims.constants.CommonConstants;
import com.tims.constants.ListResponse;
import com.tims.constants.Response;
import com.tims.model.TimsUser;
import com.tims.repository.UserRepository;
import com.tims.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public Response saveUser(TimsUser user) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		try {
			TimsUser userObjectDb = userRepository.save(user); 
			if (userObjectDb != null) {
				response.setData(userObjectDb);
				status.setMessage(CommonConstants.savedSuccessfully);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(userObjectDb);
				status.setMessage(CommonConstants.notSavedSuccessfully);
				status.setSuccess(CommonConstants.fail);
			}
			response.setStatus(status);

		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		return response;
	}

	public ListResponse getAllUsersIsActive() {
		ListResponse response = new ListResponse();
		ListResponse.Status status = new ListResponse.Status();
		try {

			List<TimsUser> userList = new ArrayList<>();
			// (List<TimsUser>) userRepository.findAllAndByIsActive(true);

			if (!userList.isEmpty() && userList.size() > 0) {
				response.setData(userList);
				status.setMessage(CommonConstants.found);
				status.setSuccess(CommonConstants.success);
			} else {
				response.setData(null);
				status.setMessage(CommonConstants.notFound);
				status.setSuccess(CommonConstants.fail);

			}
			response.setTotalResult(userList.size());

		} catch (Exception e) {
			status.setMessage(e.toString());
		}
		response.setStatus(status);
		return (response);
	}

	@Override
	public Response getUserId(int userId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		TimsUser user = userRepository.findById(userId).get();
		if (user != null) {
			response.setData(user);
			status.setMessage(CommonConstants.found);
			status.setSuccess(CommonConstants.success);
		}
		response.setStatus(status);
		return response;
	}

	@Override
	public Response deleteUserById(int deleteUserId) {
		Response response = new Response();
		Response.Status status = new Response.Status();
		userRepository.deleteById(deleteUserId);
		status.setMessage(CommonConstants.deletedSuccesfully);
		status.setSuccess(CommonConstants.success);
		response.setStatus(status);
		return response;
	}
	


}
