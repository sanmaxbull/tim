package com.tims.service;

import java.util.List;

import com.tims.constants.Response;
import com.tims.model.RegistrationCandidateDetails;

public interface CandidateService {

	Response registerCandidate(RegistrationCandidateDetails registrationCandidateDetails);

	Response getAllRegisterCandidate(Boolean isActive, Integer organisatioId, String search, Integer start,
			Integer limit);

	Response getRegistrationCandidate(Integer registrationCandidateId);

	Response deleteRegisterCandidateList(List<RegistrationCandidateDetails> registrationCandidateDetailsList);

	Response deleteRegisterCandidateId(Integer registrationCandidateId);

}
