package com.tims.service;

import com.tims.constants.ListResponse;
import com.tims.constants.Response;
import com.tims.model.TimsUser;

public interface UserService {

	Response saveUser(TimsUser user);

	ListResponse getAllUsersIsActive();

	Response getUserId(int userId);

	Response deleteUserById(int id);

}
