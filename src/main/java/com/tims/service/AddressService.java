package com.tims.service;

import java.util.List;

import com.tims.constants.ListResponse;
import com.tims.constants.Response;
import com.tims.model.Address;

public interface AddressService {

	Response saveAddress(Address address);

	Response getAddress(Integer addressId);

	Response deleteAddressIdPermanent(Integer addressId);

	Response deleteAddressList(List<Integer> addressIdList);

	ListResponse getCandidateAddressList();

}
