package com.tims.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tims.constants.Response;
import com.tims.model.RegistrationCandidateDetails;
import com.tims.service.CandidateService;

@RestController
@RequestMapping(value = { "/candidate" })
public class CandidateController {

	@Autowired
	CandidateService candidateService;

	@PostMapping(value = "/saveRegisterCandidate")
	public Response registerCandidate(@RequestBody RegistrationCandidateDetails registrationCandidateDetails) {

		return candidateService.registerCandidate(registrationCandidateDetails);
	}

	@GetMapping(value = "/getRegistrationCandidates")
	public Response getAllRegisterCandidate(@RequestParam Boolean isActive, @RequestParam Integer organisationId,
			@RequestParam String search, @RequestParam Integer start, @RequestParam Integer limit) {
		return candidateService.getAllRegisterCandidate(isActive, organisationId, search, start, limit);
	}

	@GetMapping(value = "/getRegistrationCandidate")
	public Response getRegistrationCandidate(@RequestParam Integer registrationCandidateId) {

		return candidateService.getRegistrationCandidate(registrationCandidateId);
	}

	@DeleteMapping(value = "/deleteRegisterCandidateList")
	public Response deleteRegisterCandidateList(
			@RequestParam List<RegistrationCandidateDetails> registrationCandidateDetailsList) {
		return candidateService.deleteRegisterCandidateList(registrationCandidateDetailsList);
	}

	@DeleteMapping(value = "/deleteRegisterCandidateId")
	public Response deleteRegisterCandidateId(@RequestParam Integer registrationCandidateId) {
		return candidateService.deleteRegisterCandidateId(registrationCandidateId);
	}

}
