package com.tims.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tims.constants.ListResponse;
import com.tims.constants.Response;
import com.tims.model.Address;
import com.tims.service.AddressService;

@RestController
@RequestMapping(value = { "/address" })
public class AddressController {

	@Autowired
	AddressService addressService;

	@PostMapping(value = "/save")
	public Response saveAddress(@RequestBody Address address) {
		return addressService.saveAddress(address);
	}
	
	@GetMapping(value = "/getCandidateAddressList")
	public ListResponse getCandidateAddressList() {
		return addressService.getCandidateAddressList();
	}

	@GetMapping(value = "/getCandidateAddress")
	public Response getCandidateAddress(@RequestParam(value = "addressId") Integer addressId) {
		return addressService.getAddress(addressId);
	}
	
	@DeleteMapping(value = "/deleteAddressIdPermanent")
	public Response deleteAddressIdPermanent(@RequestParam(value = "addressId") Integer addressId) {
		return addressService.deleteAddressIdPermanent(addressId);
	}
	
	@DeleteMapping(value = "/deleteAddressList")
	public Response deleteAddressList(@RequestBody List<Integer> addressIdList) {
		return addressService.deleteAddressList(addressIdList);
	}
}
